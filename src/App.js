import './App.css';
import LuckyDraw from "./components/LuckyDraw";

function App() {
  return (
    <div className='container mt-5 text-center'>
      <LuckyDraw></LuckyDraw>
    </div>
  );
}

export default App;
