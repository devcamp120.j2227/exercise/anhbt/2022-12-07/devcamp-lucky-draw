import { Component } from "react";

class LuckyDraw extends Component {
    constructor() {
        super();
        this.state = {
            ball: this.newDraw()
        }
        this.newDraw = this.newDraw.bind(this);
        this.onGenerateClick = this.onGenerateClick.bind(this);
    }
    newDraw() {
        return [
            this.random1to99(),
            this.random1to99(),
            this.random1to99(),
            this.random1to99(),
            this.random1to99(),
        ]
    }
    onGenerateClick() {
        this.setState({ball: this.newDraw()})
    }
    random1to99() {
        return Math.floor(Math.random() * 99 + 1);
    }
    render() {
        return (
            <>
                <h1>Lucky Draw</h1>
                <div className="ball-row mt-4">
                    <div className="ball">{this.state.ball[0]}</div>
                    <div className="ball">{this.state.ball[1]}</div>
                    <div className="ball">{this.state.ball[2]}</div>
                    <div className="ball">{this.state.ball[3]}</div>
                    <div className="ball">{this.state.ball[4]}</div>
                </div>
                <button className="btn btn-success mt-4" onClick={this.onGenerateClick}>Generate</button>
            </>
        )
    }
}

export default LuckyDraw